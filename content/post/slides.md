---
title: "My Slides"

date: 2018-09-19T00:00:00
# lastmod: 2018-09-09T00:00:00

#draft: false  # Is this a draft? true/false
share: false  # Show social sharing links?
profile: false  # Show author profile?
comments: false  # Show comments?

toc: true  # Show table of contents? true/false
type: "docs"  # Do not modify.

menu: post
---

[ECON 522B slides](/files/ECON522B_slides_HaoLiu.pdf)  

[ECON 522B report](/files/ECON522B_report_HaoLiu.pdf)  

[SIE 578 slides](/files/HaoLiu_SIE578_slides.pdf)  

[SIE 578 visualization](liuhoward.tk:6006)

