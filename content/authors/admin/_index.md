---
# Display name
name: Hao Liu

# Username (this should match the folder name)
authors:
- admin

# Is this the primary user of the site?
superuser: true

# Role/position
role: PhD in Management Information Systems

# Organizations/Affiliations
organizations:
- name: University of Arizona
  url: ""

# Short bio (displayed in user profile at end of posts)
bio: My research interests include Deep Learning and Recommender Systems.

interests:
- Deep Learning
- Recommender Systems

#education:
#  courses:
#  - course: PhD in Artificial Intelligence
#    institution: Stanford University
#    year: 2012
#  - course: MEng in Artificial Intelligence
#    institution: Massachusetts Institute of Technology
#    year: 2009
#  - course: BSc in Artificial Intelligence
#    institution: Massachusetts Institute of Technology
#    year: 2008

# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/widgets/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.
social:
- icon: envelope
  icon_pack: fas
  link: 'mailto:liuhao16@email.arizona.edu'  # For a direct email link, use "mailto:test@example.org".
#- icon: twitter
#  icon_pack: fab
#  link: https://twitter.com/GeorgeCushen
#- icon: google-scholar
#  icon_pack: ai
#  link: https://scholar.google.co.uk/citations?user=sIwtMXoAAAAJ
- icon: github
  icon_pack: fab
  link: https://github.com/liuhoward
# Link to a PDF of your resume/CV from the About widget.
# To enable, copy your resume/CV to `static/files/cv.pdf` and uncomment the lines below.  
- icon: cv
  icon_pack: ai
  link: files/HaoLiu_CV.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: "liuhao16@email.arizona.edu"
  
# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.  
user_groups:
- Researchers
- Visitors
---

I am currently a PhD student in the [Department of Management Information Systems](https://mis.eller.arizona.edu/), [University of Arizona](https://www.arizona.edu/) under the supervision of [Dr. Yong Ge](https://mis.eller.arizona.edu/people/yong-ge). Before coming to UoA, I earned Master degree from [UNC Charlotte](https://www.uncc.edu/). I have also gained two-year working experience at [TP-Link](https://www.tp-link.com/) as a software engineer after receiving my bachelor degree from [University of Science and Technology of China (USTC)](http://en.ustc.edu.cn/) in 2013.


